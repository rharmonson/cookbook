# Cornbread

## Ingredients

### Dry

* 1 3/4 cup (206g) all-purpose Flour
* 1 cup (156g) cornmeal
* 1/4 cup (50g) sugar
* 2 tsp baking powder
* 1/4 tsp baking soda
* 1/2 tsp salt


### Wet

* 1 1/4 cup (283g) milk or buttermilk
* 4 tbs (57g) unsalted butter
* 1/4 cup (50g) vegetable oil
* 1 large egg

### Substitutions

Substituting milk with 1 3/4 cup almond milk or other 1 3/4 cup non-diary milk substitute. If the use of diary is not a concern, add 2 to 3 tbs powder milk to improve texture and flavor.

Substituting 3/4 cup AP flour with whole wheat flour results with a better tasting cornbread but it will be denser due to the loss of gluten.

## Instructions

1. Preheat oven to 375 deg. F
2. Lightly grease
    a. 9" square or round pan or
    b. 1 1/2 quart casserole dish or
    c. 12 muffin cup pan
3. In large bowl, combine and mix the dry ingredients
4. In a large measuring cup or bowl, warm the milk and butter until the butter is melted
5. Wisk in the vegatable oil and egg to the milk/butter
6. Pour all the liquid into the bowl of dry ingredients and mix briefly--do not overmix
7. Pour into lightly greased pan
8. Bake between 20 to 25 minutes at 375 F

Cornbread is done when the top begins brown.

Longer baking will result with a satisfying crunchy exterieor but left for too long, the crumb with be dry--especially when using non-diary milk. If too dry, reduce baking time by two minutes until satisfied.
