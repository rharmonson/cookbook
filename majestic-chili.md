# Majestic chili

## Ingredients

* 1 lb Lean ground beef (optional)
* 1 can (19 oz) black beans
* 1 can (15 oz) kidney beans
* 1 can (15 oz) garbanzo beans
* 1 can (16 oz) baked beans
* 1 can (14.5 oz) omatoes; diced or stewed
* 1 can (14.5 oz) Tomatoe sauce
* 1 can (15 oz) whole kernel corn
* 1 lg onion; chopped
* 3 to 4 medium aneheim or pablano chili; chopped
* 4 to 6 cloves garlic; chopped
* 4 to 6 medium carrots; chopped
* 1 large dried ancho chilli w/seads; cut with scissors
* 1 to 2 tsp chili powder
* 2 tsp smoked salt (or salt)
* 2 tbs dried chopped onion
* 1 tbs dried parsley
* 1 tbs dried oregano
* 1 tbs dried basil
* 2 tbs sun-dried tomatoes (optional)
* 3 to 4 medium dried whole bay leaves

### Substitutions

Omit or substitute lean ground beef with 1 to 2 lbs of your preferred cut of chicken, pork, or beef or mushrooms.

Omit or substitue baked beans with pinto or kidney beans.

The differing beans add a variety of textures and subtle flavors but may be replaced with your preferred bean. Experiment!

### Chilis

The recipe has three sources of heat: aneheim and ancho chilis and chili powder. Chili powders vary greatly. Recommend omitting the chili powder and adding to taste the last hour of cooking.

## Instructions

1. Add vegetables to a 6 to 7 quart crockpot
2. Add lean ground beef
3. Add seasonings
4. Add canned vegetables and beans
5. Set crockpot to low and cook for eight to ten hours

The last hour, stir, taste, and add chili powder or additional seasoning to preference.
